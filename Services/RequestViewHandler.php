<?php

namespace ThinkCreative\CoreBundle\Services;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;

class RequestViewHandler
{

    protected $Parameters;
    protected $Controller;
    protected $isMasterRequest;

    public function __construct(array $project = array()) {
        $this->Controller = new \stdClass();
        $this->Parameters = new ParameterBag($project);
    }

    public function getController() {
        return $this->Controller;
    }

    public function getParameters() {
        return $this->Parameters->all();
    }

    public function isMasterRequest($type = NULL) {
        return (
            is_bool($type) ? ($this->isMasterRequest = $type) : $this->isMasterRequest
        );
    }

    public function processController(array $controller) {
        $this->Controller->Class = get_class( $controller[0] );
        $this->Controller->Action = $controller[1];
    }

    public function set($name, $value) {
        $this->Parameters->set(
            $name, $value
        );
    }

}
