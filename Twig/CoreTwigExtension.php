<?php

namespace ThinkCreative\CoreBundle\Twig;

use ThinkCreative\CoreBundle\Services\RequestViewHandler;

class CoreTwigExtension extends \Twig_Extension
{

    protected $RequestViewHandler;

    public function __construct(RequestViewHandler $request_view_handler) {
        $this->RequestViewHandler = $request_view_handler;
    }

    public function getGlobals() {
        return array(
            'pagedata' => $this->RequestViewHandler->getParameters(),
        );
    }

    public function getName(){
        return 'thinkcreative_core_extension';
    }

}
