<?php

namespace ThinkCreative\CoreBundle\Classes\OptionsManager;

use Symfony\Component\DependencyInjection\ParameterBag;

class OptionsHandler extends ParameterBag\ParameterBag
{

    protected $ID;
    protected $Groups;

    public function __construct(array $options = array(), array $groups = array()) {
        parent::__construct($options);
        $this->ID = spl_object_hash($this);
        if($groups) {
            $this->Groups = $groups;
        }
    }

    public function export($options = 'default', $group = false) {
        if($options !== '*') {
            if(
                is_string($options)
            ) {
                if(
                    ($group || !func_num_args()) && isset($this->Groups[$options])
                ) {
                    return $this->export($this->Groups[$options]);
                }
                $options = array($options);
            }

            foreach($options as $Name){
                if(
                    $this->has($Name)
                ) {
                    $Options[$Name] = $this->get($Name);
                }
            }

            return new self($Options);
        }
        return new self(
            $this->all(), $this->Groups
        );
    }

    public function getHandlerID() {
        return $this->ID;
    }

    public function has($name, $group = false) {
        return parent::has($name);
    }

}
