<?php

namespace ThinkCreative\CoreBundle\Classes\OptionsManager;

use Symfony\Component\DependencyInjection\ParameterBag;
use ThinkCreative\CoreBundle\Services\OptionsManager;

class OptionsSchema extends ParameterBag\ParameterBag
{

    public function __construct(OptionsManager $manager, array $configuration = array()) {
        $Schema = false;
        $isOptionsGroupsModified = false;

        if($configuration) {
            // prepend base schema configuration, should one exist
            if(
                isset($configuration['base']) && $Schema = $manager->getSchema($configuration['base'])
            ) {
                $configuration = array_replace_recursive(
                    $Schema->all(), $configuration
                );
            }

            $Options = self::Options();
            parent::__construct(
                array_merge(
                    $Options->Configuration, $configuration
                )
            );

            if(
                $this->has('groups') &&
                is_array(
                    $OptionsGroups = $this->get('groups')
                )
            ) {
                foreach($OptionsGroups as $Group => $Options) {
                    if(
                        is_string($Options) && $Options == '{base}' && $Schema
                    ) {
                        $isOptionsGroupsModified = true;
                        $OptionsGroups[$Group] = array_keys(
                            $Schema->get('schema')
                        );
                    }
                }
                if($isOptionsGroupsModified) {
                    $this->set(
                        'groups', $OptionsGroups
                    );
                }
            }
        }
    }

    public function appendSchema(array $configuration = array()) {
        foreach($configuration as $Name => $Value) {
            $this->setConfiguration(
                $Name, $Value
            );
        }
    }

    public function process($options) {
        if(!is_array($options) && $Default = $this->get('default')) {
            $options = array($Default => $options);
        }
        foreach(
            $this->get('schema') as $Name => $Configuration
        ) {
            if(
                !isset($options[$Name]) ||
                !self::isValid(
                    $Name, $options[$Name], $Configuration
                )
            ) {
                if(
                    isset($Configuration['default'])
                ) {
                    $options[$Name] = $Configuration['default'];
                }
            }
        }
        return new OptionsHandler(
            $options, $this->get('groups')
        );
    }

    public static function Options(){
        return (object) array(
            'Configuration' => array(
                'base' => '',
                'default' => '',
                'groups' => array(),
                'schema' => array(),
            ),
            'Group' => array(
                'trigger' => '',
                'options' => array(),
            )
        );
    }

    public function setConfiguration($name, $configuration) {
        switch($name) {
            case 'base':
            case 'default': {
                $this->set(
                    $name, $configuration
                );
                break;
            }
            case 'groups': {
                $this->set(
                    $name,
                    array_merge_recursive(
                        $this->parameters[$name], $configuration
                    )
                );
                break;
            }
            case 'schema': {
                foreach($configuration as $Name => $Settings) {
                    if(
                        isset($this->parameters[$name][$Name])
                    ) {
                        $this->parameters[$name][$Name] = array_replace_recursive(
                            $this->parameters[$name][$Name], $Settings
                        );
                        continue;
                    }
                    $this->parameters[$name][$Name] = $Settings;
                }
                break;
            }
        }
    }

    protected static function isValid($option, $value, $configuration) {
        $Type = (
            is_string($configuration['type']) ?
            preg_split(
                '/\s*\|\s*/', $configuration['type']
            ) :
            $configuration['type']
        );
        if(
            is_array($Type) &&
            in_array(
                gettype($value), $Type
            )
        ) {
            return (
                (
                    isset($configuration['values']) && is_array($configuration['values'])
                ) ?
                in_array($value, $configuration['values']) : true
            );
        }
        return false;
    }

}
