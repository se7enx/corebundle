<?php

namespace ThinkCreative\CoreBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ThinkCreativeCoreBundle extends Bundle
{

    protected $name = "ThinkCreativeCoreBundle";

}
